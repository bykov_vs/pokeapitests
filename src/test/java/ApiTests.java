import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import models.Pokemon;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static util.PokemonHelper.isWeightsLess;
import static util.PokemonHelper.getCountPokemonsWithName;
import static util.PokemonHelper.containsSpell;
import static util.Requests.getLimitPokemons;
import static util.Requests.getPokemon;

@Epic("Api Tests")
public class ApiTests {
    @Test
    @DisplayName("Сравнение двух покемонов")
    @Description("Сравнение покемонов по весу и наличию способности \"run-away\" ")
    public void pokemonCompareTest() {
        String spell = "run-away";
        Pokemon rattata = getPokemon("rattata");
        Pokemon pidgeotto = getPokemon("pidgeotto");
        assertAll(
                //Вес покемона rattata меньше веса покемона pidgeotto
                () -> assertTrue(isWeightsLess(rattata, pidgeotto),
                        "Вес первого покемона оказался больше веса второго!"),
                //Покемон rattata имеет способность run-away
                () -> assertTrue(containsSpell(rattata, spell),
                        "Покемон не содержит заданную способность!"),
                //Покемон pidgeotto не имеет способность run-away
                () -> assertFalse(containsSpell(pidgeotto, spell),
                        "Покемон содержит заданную способность!")
        );
    }

    @Test
    @DisplayName("Проверка запроса на покемонов")
    @Description("Проверка запроса на покемонов с ограничением количества получаемых записей")
    public void pokemonRecordsLimitTest() {
        int expected = 10;
        Pokemon[] pokemons = getLimitPokemons(expected);
        int actualPokemons = pokemons.length;
        long actualNames = getCountPokemonsWithName(pokemons);
        assertAll(
                //Покемонов нашлось столько же, сколько и запрашивали
                () -> assertEquals(expected, actualPokemons,
                        "Количество полученных покемонов отличается от запрашиваемого числа!"),
                //Количество покемонов с именем столько же, сколько и нашлось
                () -> assertEquals(expected, actualNames,
                        "Количество покемонов с именем не соответсвует числу запрашиваемых покемонов!")
        );
    }
}
