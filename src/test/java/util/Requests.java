package util;

import io.qameta.allure.Step;
import io.restassured.path.json.config.JsonPathConfig;
import models.Pokemon;

import static io.restassured.RestAssured.given;
import static util.Paths.basePath;
import static util.Paths.pokemonPath;
import static util.Specs.baseRespSpec;
import static util.Specs.pokemonReqSpec;

public class Requests {
    @Step("Получение покемона {name}")
    public static Pokemon getPokemon(String name) {
        return given()
                    .spec(pokemonReqSpec)
                    .pathParam("name", name)
                .when()
                    .get(pokemonPath)
                .then()
                    .spec(baseRespSpec)
                    .extract()
                    .as(Pokemon.class);
    }

    @Step("Получение списка покемонов в количестве {limit}")
    public static Pokemon[] getLimitPokemons(int limit) {
        return given()
                    .spec(pokemonReqSpec)
                    .queryParam("limit", limit)
                .when()
                    .get(basePath)
                .then()
                    .spec(baseRespSpec)
                    .extract()
                    .jsonPath(JsonPathConfig.jsonPathConfig())
                    .getObject("results", Pokemon[].class);

    }
}
