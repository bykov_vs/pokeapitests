package util;

public class Paths {
    public static final String baseUri = "https://pokeapi.co/api/v2";
    public static final String basePath = "/{category}/";
    public static final String pokemonPath = "/{category}/{name}/";
}
