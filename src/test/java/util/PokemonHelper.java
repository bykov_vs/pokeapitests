package util;

import io.qameta.allure.Step;
import models.Pokemon;

import java.util.Arrays;

public class PokemonHelper {
    @Step("Поиск способности {spell} у {pokemon.name}")
    public static boolean containsSpell(Pokemon pokemon, String spell) {
        return pokemon.getAbilityNames().contains(spell);
    }

    @Step("Сравнение весов покемонов {pokemon1.name} и {pokemon2.name}")
    public static boolean isWeightsLess(Pokemon pokemon1, Pokemon pokemon2) {
        return pokemon1.getWeight() < pokemon2.getWeight();
    }

    @Step("Получение количества покемонов с именем")
    public static long getCountPokemonsWithName(Pokemon[] pokemons) {
        return Arrays.stream(pokemons).filter(pokemon -> pokemon.getName() != null).count();
    }
}
