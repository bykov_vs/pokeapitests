package util;

import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class Specs {
    public static ResponseSpecification baseRespSpec = new ResponseSpecBuilder()
            .expectStatusCode(200)
            .build();
    public static RequestSpecification pokemonReqSpec = new RequestSpecBuilder()
            .setBaseUri(Paths.baseUri)
            .addFilter(new AllureRestAssured())
            .setAccept(ContentType.JSON)
            .addPathParam("category", "pokemon")
            .build();
}
