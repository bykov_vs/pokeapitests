package models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.stream.Collectors;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Pokemon {
    private String name;
    private int weight;

    @JsonProperty("abilities")
    private List<Ability> abilities;

    public Pokemon(String name, int weight) {
        this.name = name;
        this.weight = weight;
    }

    public Pokemon() {
    }

    public List<String> getAbilityNames() {
        return this.abilities.stream()
                .map(Ability::getName)
                .collect(Collectors.toList());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public List<Ability> getAbilities() {
        return abilities;
    }
}
